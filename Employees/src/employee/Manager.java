/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

/**
 *
 * @author lmangoua
 */
public class Manager extends Employee {
    
    //Instance declaration
    private String name;
    private double salary;
    private double bonus;
    
    //Constructor
    public Manager() {
    }

    public Manager(String name, double salary, double bonus) {
        this.name = name;
        this.salary = salary;
        this.bonus = bonus;
    }
    
    //Getter & Setter
    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

}
